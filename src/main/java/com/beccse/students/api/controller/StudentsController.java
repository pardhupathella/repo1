package com.beccse.students.api.controller;

import com.beccse.students.api.domain.Student;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/test/students")
public class StudentsController {
    List<Student> students;
    public StudentsController() {
        students = new ArrayList<>();
        students.add(new Student("1", "abc", "xyz"));
        students.add(new Student("2", "def", "uvw"));
   }

    @GetMapping(value="/{id}")
    public Student getStudent(@PathVariable String id)
    {
        Optional<Student> student=students.stream().filter(x->x.getStudentid().equalsIgnoreCase(id)).findFirst();
        return student.get();
    }
    @PostMapping
    public String saveStudent(@RequestBody Student student)
    {
        students.add(student);
        return "Successfully Student Info Saved";
    }
    @PutMapping
    public String editStudent(@RequestBody Student student)
    {
        Student student11=getStudent(student.getStudentid());
        student11.setFirstName(student.getFirstName());
        student11.setLastName(student.getLastName());
        return "Successfully Student Info Edited";
    }
}
