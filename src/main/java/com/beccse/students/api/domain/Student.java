package com.beccse.students.api.domain;

public class Student {
    private String studentid;
    private String firstName;
    private String lastName;

    public Student(){

    }
    public Student(String studentid, String firstName, String lastName) {
        this.studentid = studentid;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
